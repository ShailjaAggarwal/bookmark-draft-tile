import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  TextInput,
  Platform,
  Dimensions,
  PixelRatio
} from 'react-native';
import {connect} from 'react-redux';
import MyIcon from 'react-native-vector-icons/Ionicons';
import ScalableText from 'react-native-text';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
let pixelRatio = PixelRatio.get();


const mapStateToProps = (state) => {
  return {

  }
}


const mapDispatchToProps = (dispatch) => {
  return {}
}


class BookmarkAndDraft extends React.Component {

  static defaultProps={
    containerStyle:{
      borderWidth:1,
      width:(width-58)/2,
      borderColor:'#d7d7d7',
      shadowColor :'rgba(0,0,0,0.05)',
      shadowOffset: { width: 0, height: 2, },
      shadowOpacity: 1,
      shadowRadius: 10,
      borderRadius:10,
      marginVertical:10,
    },
    iconContainer:{
      height:height*.15,
      justifyContent:'center',
      alignItems:'center'
    },
    icontype:'',
    iconStyle:{fontSize:40,marginTop:20},
    title:'',
    type:'',
    titleTextStyle:{
      color:'black',
      fontWeight:'bold',
      fontSize:16,
      fontFamily:'PingFang SC',
    },
    titleViewStyle:{
      alignItems:'center',
      justifyContent:'center'
    },
    typeTextStyle:{
      color:'black',
      fontSize:12,
      fontFamily:'PingFang SC',
    },
    typeViewStyle:{
      alignItems:'center',
      justifyContent:'center',
      marginBottom:10
    },
    action:()=>{}
  }

  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentWillMount(){
  }

  render(){
    let {
      collectionNumber,
      containerStyle,
      iconContainer,
      icontype,
      iconStyle,
      title,
      type,
      titleTextStyle,
      titleViewStyle,
      typeTextStyle,
      typeViewStyle,
      action
    }=this.props;
    let widthCollection=0
    if(pixelRatio==3){
      widthCollection=.425
  }
  else if(pixelRatio==2){
    widthCollection=.41

  }
return(

  <TouchableOpacity style={containerStyle}
    onPress={()=>{
      action()
    }}
  >

    <View style={[{flex:1},iconContainer]}>
      <MyIcon name={icontype} style={iconStyle}/>
    </View>

    <View style={{flex:1}}>

      <View style={[{flex:2},titleViewStyle]}>
        <ScalableText style={titleTextStyle}>{title}</ScalableText>
      </View>

      <View style={typeViewStyle}>
        <ScalableText style={[{flex:1},typeTextStyle]}>{type}</ScalableText>
      </View>

    </View>

  </TouchableOpacity>

)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookmarkAndDraft)
